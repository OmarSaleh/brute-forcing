const calculateHash = (str) => {
    let hash = str
        .split("")
        .map((c, i) => str.charCodeAt(i))
        .map((c) => c + 2)
        .map((c) => String.fromCharCode(c))
        .join("");
    return Buffer.from(hash).toString("base64");
};
// definition de toutes les lettres possible
const letters = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
];

// Test d'appel de certains modules (non def à ce moment)
const fs = require("fs");

const readline = require("readline");

// Lecture du stream
const rl = readline.createInterface({
    input: fs.createReadStream("list.txt"),
    output: process.stdout,
});

const hash = "ZWpxZQ==";

for (let i0 = 0; i0 < letters.length; i0++) {
    for (let i1 = 0; i1 < letters.length; i1++) {
        for (let i2 = 0; i2 < letters.length; i2++) {
            for (let i3 = 0; i3 < letters.length; i3++) {
                const combinaison =
                    letters[i0] + letters[i1] + letters[i2] + letters[i3];
                const combinHash = calculateHash(combinaison);
                if (combinHash === hash) {
                    console.log(`Voici ton string ${combinaison}`);
                    break;
                }
            }
        }
    }
}